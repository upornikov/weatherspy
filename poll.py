#!/usr/bin/env python

# for various python versions
try:
    import reload
except:
    try:
        from imp import reload as reload
    except:
        from importlib import reload as reload
    
import os, time
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'weatherspy.settings')

import django
django.setup()

from weatherspy import settings
from app.views import update_forecasts
from app.models import Forecast

# default forecast update interval
interval = 0
default_interval = 2
last_update_time = 0

def check_for_update(): 
    reload(settings)  #reload new value, if file is updated 
    global interval, last_update_time
    try:
        new_interval = round(settings.FORECAST_UPDATE_INTERVAL)
    except:
        new_interval = default_interval

    if new_interval < default_interval:
        new_interval = default_interval

    if new_interval != interval:
        interval = new_interval
        print("Updating forecasts every %d minutes... (Ctrl+C to stop)"%interval)
	
    cur_time = time.time()
    if cur_time > last_update_time+60*interval:
        update_forecasts()
        last_update_time = time.time()
	

while True:
    check_for_update()
    time.sleep(5)
print('Quit')
exit(0)

    
    