#!/usr/bin/env python

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'weatherspy.settings')

import django
django.setup()

from app.models import TemperatureRange, Forecast

#every new location to watch gets its TemperatureRange object
#alt: populate TemperatureRange objects from files, UI widget or admin interface

sample_locations = ['Moscow,RU', 'helsinki,fi', 'London,UK']
sample_temp_ranges_per_month = [(-5,5),(-7,3),(-3,7),(-1,9),(1,11),(3,13),(7,17),(3,13),(1,11),(-1,9),(-3,7),(-5,5)]

Forecast.objects.all().delete()
TemperatureRange.objects.all().delete()

for loc in sample_locations:
    tr = TemperatureRange.objects.create(tag=loc)
    for i, r in enumerate(sample_temp_ranges_per_month):
        tr.set_monthly_range(i+1, 31*[r])

print('Populated')

    
    