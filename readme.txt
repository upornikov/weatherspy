Design notes.
============

The WeatherSpy is a web application. 

It holds locations to monitor with limit temperatures, in the form of location data and (min_temp_limit, max_temp_limit) pair per day.

For registered locations, it reads 5-days temperature forecast from  www://openweathermap.org. From hourly forecast, it calculates min/max temperatures per day and matches it with corresponding limit temperatures.  Resulting forecast sums up a structure [(min_temp, min_temp_deviation), (max_temp, max_temp_deviation), alert_flag] per day. The daily alert is raised, if either min_temp_deviation<0, or max_temp_deviation>0. The 5-days forecast alert is a logical sum of the daily alerts.

The forecasts are listed in www://weatherspy_address page.

The forecasts are re-calculated from www://openweathermap.org, when navigating go www://weatherspy_address/update. There is a minimal interval between re-calculations (2 minutes) to prevent congestion (skipped for demo purposes). 

To have the WeatherSpy service, run poller application (python poller.py), which will periodically call for forecast updates with an interval stored in the application settings file. The interval can be dynamically modified with minimal value of 2 min.

With every update, forecast data is logged to the log file (currently ./weatherspy.log). Log file location is configurable via the settings file. 



Setup:
======

Have python installed: development and testing was done with python 3.6 - other versions were not checked. After checking out source code, install needed runtime by running from the project root:
> pip install -r requirements.txt

Locations with temperature ranges to monitor and forecasts are stored in a db as TemperatureRange and Forecast models in sqllite db coming with django. Initiate the db:
> python manage.py makemigrations app
> python manage.py migrate

Start the WeatherSpy web app by running it on a django development server:
> python manage.py runserver 0.0.0.0:80

There is no data in the database: check http://localhost/ to see no forecasts yet. To populate db with sample temperature ranges (sample locations), run:
> python populate.py

This will create 3 sample temperature ranges for Helsinki, London and Moscow. Alternatively, new locations to monitor can be imported with temperature ranges from corresponding files (e.g. CSV), or via web widget.

Check http://localhost/ - there are no Forecasts yet.

You can also view/modify database contents via admin interface. First create db superuser:
> python manage.py createsuperuser
Then login as the superuser via http://localhost/admin to manipulate Forecasts and TemperatureRange models from there.

Create/update weather forecasts by browsing to http://localhost/update. 

You can also use weather poller application to continuously update forecasts:
> python poller.py

The forecast update interval (in minutes) is configured in settings file (./weatherspy/settings.py) as FORECAST_UPDATE_INTERVAL value.

The WeatherSpy app logs updated forecasts via ./weatherspy.log file. The file location, size, number of backups and output format settings are configured via the settings file too.



Notification Service Notes.
===========================

Notification service can be implemented via subscription model.

User subscribes for alert notificaitons from certain locations via his email. User profile with user credentials is created herewith.

The location stores subscriber ids as foreign keys. When alert status for the location switches from False to True, emails are sent to the subscribers.

In turn, user profile stores location ids as foreing keys, for browsing/manipulating subscriptions.

User authentication can be implemented via SingleSignOn model, where user authenticity is certified by a social platform authentication service (a little bit more secure option), or via sending single-time access tokens to user's address specified at SignOn.

