from django.shortcuts import render, redirect
from app.models import Forecast, TemperatureRange
import json
from urllib.request import urlopen


forecaster = 'http://openweathermap.org/data/2.5/forecast'
app_id = 'b6907d289e10d714a6e88b30761fae22'

def index(request):
    forecasts = Forecast.objects.all()
    #forecast format is too complex for simple HTML templates:
    #re-format forecast dict to show on a page
    for f in forecasts:
        f.forecast = json.loads(f.forecast)
        for n, v in f.forecast.items():
            s = '{0}({1}) : {2}({3})'.format(v[0][0], v[0][1], v[1][0], v[1][1])
            if v[2]:    # mark alerted forecast with *
                s += ' *'
            f.forecast[n] = s
    dates = {} if not forecasts else forecasts[0].forecast.keys()
    return render(request, 'app/forecasts.html', {'forecast_list': forecasts, 'dates': dates})


def update_forecast(tag, data):
    obj = Forecast.objects.get_or_create(tag=tag)[0]
    obj.update(data)

def update_forecasts():
    tags = TemperatureRange.objects.values_list('tag', flat=True)
    for tag in tags:
        url = '{0}?q={1}&appid={2}'.format(forecaster, tag, app_id)
        update_forecast(tag, json.loads(urlopen(url).read()))

def update(request):
    """Update forecasts for all locations with TemperatureRange defined"""
    if request.method == 'GET':
        update_forecasts()
    return redirect('/')

