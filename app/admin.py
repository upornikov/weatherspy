from django.contrib import admin

from app.models import TemperatureRange, Forecast

admin.site.register(TemperatureRange)
admin.site.register(Forecast)
