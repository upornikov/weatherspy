from django.conf.urls import url
from app import views

urlpatterns = [
        url(r'^$', views.index, name='weatherspy_index'),
		url(r'^update/$', views.update, name='update_all_forecasts'),
]
