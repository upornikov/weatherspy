from django.db import models
import json
import logging

logger = logging.getLogger('forecast')

class Forecast(models.Model):
    #ISO-3166: London,uk Helsinki,fi Moscow,ru ...
    tag = models.CharField(max_length=50, blank=False, verbose_name='CityName.ContryCode in ISO-3166', primary_key=True)
    #json of 5-days forecast
    #forecast sample: {
    #   name='Helsinki', country='FI',
    #   forecast=[{'YYYY-MM-DD': [(t_min, t_min_variation), (t_max, t_max_variation), Alert]}, ...]
    #}, 
    #where temp_*=(temperature, deviation_from_limit);
    forecast = models.CharField(max_length=500, blank=True, default='', verbose_name='Weather Forecast with Alert Status')
    #flag to show if the forecast has an alert
    alerted = models.BooleanField(default=False, verbose_name="Forecast Alerts")
    #use last_updated value to avoid updating too often (minimal interval is 1 minute)
    ##skipped for this demo
    last_updated = models.DateTimeField(blank=True, null=True, default=None)
    city_name = models.CharField(max_length=50, blank=True, null=False, verbose_name='City Name')
    country_code = models.CharField(max_length=5, blank=True, null=False, verbose_name='Country Code')

    def __init__(self, *args, **kwargs):
        super(Forecast, self).__init__(*args, **kwargs)
        self.tag = self.tag.lower()
        t = self.tag.split(',')
        self.city_name = '.'.join(t[:-1]).capitalize()
        self.country_code = t[-1].upper()
    def __str__(self):
        return self.tag + ' forecast'
        
    def update(self, data):
        """data: forecast dict from weather provider"""
        
        ##skipped for debugging; 
        #check self.last_updated and do not update, if less then <min_interval> has passed since last update;
        
        forecast = dict()
        #collect daily min/max temperatures for the forecast
        for f in data['list']:
            date = f['dt_txt'].split()[0]
            if date not in forecast:
                t_min = 1000
                t_max = -1000
            t_min = min(t_min, f['main']['temp_min'])
            t_max = max(t_max, f['main']['temp_max'])
            forecast[date] = t_min, t_max
            
        tr_obj = TemperatureRange.objects.get(tag=self.tag)
        self.alerted = False
        temp_range = dict()
        forecast_var = dict()
        #collect daily min/max temperature variations for the forecast, set alert if needed
        for d, (t_min, t_max) in forecast.items():
            _, month, day = d.split('-')
            if month not in temp_range:
                temp_range[month] = json.loads(tr_obj.get_month_temp_range(month))
            t_min_var = t_min - temp_range[month][int(day)-1][0] 
            t_max_var = t_max - temp_range[month][int(day)-1][1]
            t_alert = t_min_var<0 or t_max_var>0
            #set alert on the forecast, if at least a single day's temp. falls out of temp.range;
            self.alerted = True if t_alert else self.alerted
            forecast_var[d] = t_min_var, t_max_var, t_alert

        res = dict()
        #collect the forecast: ((t_min, t_min_var), (t_max, t_max_var), alert_here)    
        for d in forecast:
            res[d] = (round(forecast[d][0]), round(forecast_var[d][0])), \
                     (round(forecast[d][1]), round(forecast_var[d][1])), \
                     forecast_var[d][2]  
        self.forecast = json.dumps(res)
        self.save()
        logger.info("%s %s %s"%(self.tag, self.alerted, self.forecast))
    
    class Meta:
        ordering = ('tag', 'forecast',)
        

class TemperatureRange(models.Model):
    #ISO-3166: London,uk Helsinki,fi Moscow,ru ...
    tag = models.CharField(max_length=50, blank=False, verbose_name='CityName.ContryCode in ISO-3166', primary_key=True)
    #monthly ranges (json): [(temp_min, temp_max), ...]
    range_01 = models.CharField(max_length=400, verbose_name='January Weather Range')
    range_02 = models.CharField(max_length=400, verbose_name='February Weather Range')
    range_03 = models.CharField(max_length=400, verbose_name='March Weather Range')
    range_04 = models.CharField(max_length=400, verbose_name='April Weather Range')
    range_05 = models.CharField(max_length=400, verbose_name='May Weather Range')
    range_06 = models.CharField(max_length=400, verbose_name='June Weather Range')
    range_07 = models.CharField(max_length=400, verbose_name='July Weather Range')
    range_08 = models.CharField(max_length=400, verbose_name='August Weather Range')
    range_09 = models.CharField(max_length=400, verbose_name='September Weather Range')
    range_10 = models.CharField(max_length=400, verbose_name='October Weather Range')
    range_11 = models.CharField(max_length=400, verbose_name='November Weather Range')
    range_12 = models.CharField(max_length=400, verbose_name='December Weather Range')
    city_name = models.CharField(max_length=50, blank=True, null=False, verbose_name='City Name')
    country_code = models.CharField(max_length=5, blank=True, null=False, verbose_name='Country Code')

    def __init__(self, *args, **kwargs):
        super(TemperatureRange, self).__init__(*args, **kwargs)
        self.tag = self.tag.lower()
        t = self.tag.split(',')
        self.city_name = '.'.join(t[:-1]).capitalize()
        self.country_code = t[-1].upper()
    def __str__(self):
        return self.tag + ' weather range'
    def get_month_temp_range(self, month_number):
        return self.__getattribute__('range_%02d'%int(month_number))
        
    #helper functions to populate ranges;
    def set_monthly_range(self, month_number, data):
        """data: [(temp_min, temp_max), ...]"""
        assert int(month_number)>0 and int(month_number)<13, 'Invalid month number'
        self.__setattr__('range_%02d'%month_number, json.dumps(data))
        super(TemperatureRange, self).save()
        
    class Meta:
        ordering = ('tag', )
    